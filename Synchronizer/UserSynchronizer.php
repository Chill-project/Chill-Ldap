<?php

/*
 * Chill is a software for social workers
 * Copyright (C) 2016 Champs Libres <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Chill\LdapBundle\Synchronizer;

use Symfony\Component\Ldap\Ldap;
use Doctrine\ORM\EntityRepository;
use Psr\Log\LoggerInterface;
use Symfony\Component\Ldap\Entry;
use Chill\MainBundle\Entity\User;
use Chill\LdapBundle\Entity\UserLdapBinding;
use Chill\LdapBundle\Client\ClientFactory;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Synchronize the ldap directory with users in Chill
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 * @author Champs Libres <info@champs-libres.coop>
 */
class UserSynchronizer
{
    /**
     *
     * @var type 
     */
    protected $ldapFactory;
    
    /**
     * The LDAP connector
     *
     * @var Ldap
     */
    protected $ldap;
    
    /**
     * The repository for User
     *
     * @var EntityRepository
     */
    protected $userRepository;
    
    /**
     * The repository for user binding
     *
     * @var EntityRepository
     */
    protected $userBindingRepository;
    
    /**
     *
     * @var ObjectManager
     */
    protected $om;
    
    /**
     *
     * @var LoggerInterface
     */
    protected $logger;
    
    /**
     * the DN where the query is executed
     *
     * @var string
     */
    protected $dn = "dc=champs-libres,dc=coop";
    
    /**
     * The query to retrieve people
     *
     * @var string
     */
    protected $query = "(&(objectClass=inetOrgPerson)(userPassword=*))";
    
    /**
     * the attribute which contains login
     *
     * @var string 
     */
    protected $usernameAttr = 'cn';
    
    /**
     * Store if the object manager need a flush.
     *
     * @var boolean
     */
    protected $needFlush = false;


    public function __construct(
          ClientFactory $ldapFactory,
          EntityRepository $userRepository,
          EntityRepository $userBindingRepository,
          ObjectManager $om,
          LoggerInterface $logger,
          $dn,
          $query,
          $usernameAttr
          )
    {
        $this->ldapFactory = $ldapFactory;
        $this->userRepository = $userRepository;
        $this->userBindingRepository = $userBindingRepository;
        $this->logger = $logger;
        $this->om = $om;
        $this->dn = $dn;
        $this->query = $query;
        $this->usernameAttr = $usernameAttr;
    }
    
    public function synchronize() 
    {
        $this->ldap = $this->ldapFactory->getBindedClient();
        
        $this->logger->debug(sprintf('Make a ldap query for dn "%s" and '
              . 'filter "%s"', $this->dn, $this->query));
        
        /* @var $entries \Symfony\Component\Ldap\Adapter\CollectionInterface */
        $entries = $this->ldap->query($this->dn, $this->query)->execute();
        
        $this->logger->info(sprintf("Receiving %d entries from LDAP", 
              count($entries)));
        
        // collect all the DN in a single array
        $dns = array_map(function(Entry $e) { return $e->getDn(); }, $entries->toArray());
        // query to get all the user with the DN retrieved from ldap
        $query = $this->userBindingRepository
              ->createQueryBuilder('ub');
        $query->where($query->expr()->in('ub.ldap_dn', ':dns'))
              ->setParameter('dns', $dns);
        $userBindings = $query->getQuery()->getResult();
        
        /* @var $entry \Symfony\Component\Ldap\Entry */
        foreach($entries as $entry) {
            $this->logger->debug(sprintf("Processing user with dn %s", $entry->getDn()));
            
            // search amongst the user searched the one with the correct dn
            $userBinds = array_filter(
                  $userBindings,
                  function(UserLdapBinding $u) use ($entry) { 
                
                        return $entry->getDn() === $u->getLdapDn();
                  });
            
            if (count($userBinds) === 0) {
                $this->logger->debug(sprintf(
                      'The dn "%s" is not present in DB, create an user',
                      $entry->getDn()
                      ));
                $this->createUser($entry);
            } elseif (count($userBinds) === 1) {
                $this->logger->debug(sprintf(
                      'a user binded with dn "%s" exists',
                      $entry->getDn()
                      ));
                $this->activateUser($userBinds[0]->getUser());
            } else {
                throw new \RuntimeException("It seems that there is more than "
                      . "one user with the same dn!");
            }
        }
        
        // desactivate users not in ldap
        $queryInexistingUsers = $this->userBindingRepository
              ->createQueryBuilder('ub');
        
        if (count($dns) > 0) {
            $queryInexistingUsers
                  ->where($queryInexistingUsers->expr()->notIn('ub.ldap_dn', ':dns'))
                  ->setParameter('dns', $dns);
        }
        $inexistingUser = $queryInexistingUsers->getQuery()->getResult();

        /* @var $userBinded UserLdapBinding */
        foreach($inexistingUser as $userBinded) {
            $this->desactivateUser($userBinded->getUser());
        }
        
        if ($this->needFlush) {
            $this->om->flush();
        }
    }
    
    /**
     * Create a user binded with the dn defined in $entry
     * 
     * @param Entry $entry
     */
    protected function createUser(Entry $entry)
    {
        $username = $this->getAttribute($entry, $this->usernameAttr);
        
        // check an user with same username does not exists
        $user = $this->userRepository
              ->findOneBy(array('username' => $username));
        
        if ($user === null) {
            $user = (new User())
                  ->setUsername($username)
                  ->setPassword('0')
                  ;
            $this->om->persist($user);
        }
        
        $userBinding = (new UserLdapBinding())
              ->setLdapDn($entry->getDn())
              ->setUser($user);
        $this->om->persist($userBinding);
        
        $this->logger->debug(sprintf(
              'Inserting user with dn "%s" and username "%s"',
              $entry->getDn(),
              $username
              ));
        
        $this->needFlush = true;
    }
    
    /**
     * activate an user if required
     * 
     * @param User $user
     */
    protected function activateUser(User $user)
    {
        if (! $user->isEnabled()) {
            $user->setEnabled(true);
            $this->needFlush = true;
        }
    }
    
    /**
     * desactivate the $user if required
     * 
     * @param User $user
     */
    protected function desactivateUser(User $user)
    {
        if ($user->isEnabled()) {
            $user->setEnabled(false);
            $this->needFlush = true;
        }
    }
    
    /**
     * 
     * @param Entry $entry
     * @param string $name
     * @return string
     */
    protected function getAttribute(Entry $entry, $name)
    {
        $attr = $entry->getAttribute($name);
        
        if ($attr === null) {
            throw new \RuntimeException(
                  sprintf(
                        'The entry with dn "%s" has no attribute "%s"',
                        $entry->getDn(),
                        $name
                        )
                  );
        }
        
        return $attr[0];
    }
    
    
}
