<?php

/*
 * Chill is a software for social workers
 * Copyright (C) 2016 Champs Libres <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Chill\LdapBundle\Command;

use Chill\LdapBundle\Synchronizer\UserSynchronizer;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Synchronize users within Chill from ldap
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 * @author Champs Libres <info@champs-libres.coop>
 */
class SynchronizeLdapCommand extends Command
{
    /**
     *
     * @var UserSynchronizer
     */
    protected $userSynchronizer;
    
    const NAME = "chill:ldap:synchronize";
    
    public function __construct(UserSynchronizer $synchronizer) 
    {
        $this->userSynchronizer = $synchronizer;
        
        parent::__construct();
    }
    
    protected function configure()
    {
        $this->setName(self::NAME)
              ->setDescription("Synchronize users from the ldap directory with the "
                    . "chill database")
              ;
    }
    
    public function run(InputInterface $input, OutputInterface $output)
    {
        $this->userSynchronizer->synchronize();
    }
}
