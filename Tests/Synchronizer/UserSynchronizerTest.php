<?php

/*
 * Chill is a software for social workers
 * Copyright (C) 2016 Champs Libres <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Chill\LdapBundle\Tests\Synchronizer;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Prophecy\Argument;
use Symfony\Component\Ldap\Adapter\QueryInterface;
use Symfony\Component\Ldap\LdapInterface;
use Symfony\Component\Ldap\Entry;
use Symfony\Component\Ldap\Adapter\ExtLdap\Collection;
use Chill\LdapBundle\Client\ClientFactory;

/**
 * Test the user synchronizer
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 * @author Champs Libres <info@champs-libres.coop>
 */
class UserSynchronizerTest extends KernelTestCase
{
    /**
     * The repository for User
     *
     * @var EntityRepository
     */
    protected $userRepository;
    
    /**
     * The repository for user binding
     *
     * @var EntityRepository
     */
    protected $userBindingRepository;
    
    /**
     *
     * @var ObjectManager
     */
    protected $om;
    
    /**
     *
     * @var LoggerInterface
     */
    protected $logger;
    
    /**
     *
     * @var type \Prophecy\Prophet
     */
    protected $prophet;
    
    const DN = 'dn';
    
    const QUERY = 'query';
    
    const USERNAME_ATTR = 'cn';
    
    const FAKE_DN = 'uid=test,dc=example,dc=com';
    
    const FAKE_LOGIN = 'ldap login';


    public function setUp()
    {
        self::bootKernel();
        
        $container = self::$kernel->getContainer();
        
        $this->userBindingRepository = $container
              ->get('chill.ldap.user_binding_repository');
        $this->userRepository = $container
              ->get('chill.main.user_repository');
        $this->om = $container
              ->get('doctrine.orm.entity_manager');
        $this->logger = $container
              ->get('logger');
        
        $this->prophet = new \Prophecy\Prophet();
    }
    
    public function tearDown()
    {
        $em = self::$kernel->getContainer()->get('doctrine.orm.entity_manager');
        
        $em->createQuery('DELETE FROM ChillLdapBundle:UserLdapBinding ub WHERE '
              . 'ub.ldap_dn in (:dn)')
              ->setParameter('dn', array(self::FAKE_DN, self::FAKE_DN.'_2'))
              ->execute();
        $em->createQuery('DELETE FROM ChillMainBundle:User u '
              . 'WHERE u.username in (:username)')
              ->setParameter('username', array(self::FAKE_LOGIN, self::FAKE_LOGIN.'_2'))
              ->execute();
    }
    
    /**
     * Create a ldap factory, where all queries will return 
     * a collection of $entries
     * 
     * @param array $entries
     * @return ClientFactory
     */
    protected function createLdapFactory(array $entries)
    {
        // create the collection of result
        $collection = $this->prophet->prophesize();
        $collection->willExtend(Collection::class);
        $collection->toArray()->willReturn($entries);
        $collection->count()->willReturn(count($entries));
        $collection->getIterator()->willReturn(new \ArrayIterator($entries));
        
        // create the QueryInterface
        $query = $this->prophet->prophesize();
        $query->willImplement(QueryInterface::class);
        $query->execute()->willReturn($collection);
        
        //create the ldap connected
        $ldap = $this->prophet->prophesize();
        $ldap->willImplement(LdapInterface::class);
        $ldap->bind(Argument::any(), Argument::any())->willReturn(null);
        $ldap->query(Argument::type('string'), Argument::type('string'), 
              Argument::cetera())->willReturn($query->reveal());
        $ldap->getEntryManager()->willReturn(null);
        $ldap->escape(Argument::type('string'), Argument::type('string'),
              Argument::type('int'))->willReturn(null);
        
        // create the ldap factory
        $factory = $this->prophet->prophesize();
        $factory->willExtend(ClientFactory::class);
        $factory->getBindedClient()->willReturn($ldap->reveal());
        
        return $factory->reveal();
    }
    
    /**
     * 
     * @param ClientFactory $clientFactory
     * @return \Chill\LdapBundle\Synchronizer\UserSynchronizer
     */
    protected function createUserSynchronizer(ClientFactory $clientFactory)
    {
        return new \Chill\LdapBundle\Synchronizer\UserSynchronizer(
              $clientFactory,
              $this->userRepository,
              $this->userBindingRepository,
              $this->om,
              $this->logger,
              self::DN,
              self::QUERY,
              self::USERNAME_ATTR
              );
    }
    
    /**
     * Test that :
     * 
     * 1. a new user in the ldap is  persisted into the database
     * 2. if the user is not modified; the user is still present
     * 3. if the user disappeared in ldap, the user is desactivated in database
     */
    public function testUserLifecycle()
    {
        // 1. a new user in the ldap is persisted in the database
        $entry = new Entry(self::FAKE_DN, array(
           self::USERNAME_ATTR => [ self::FAKE_LOGIN ]
        ));
        
        $userSynchronizer = $this->createUserSynchronizer(
              $this->createLdapFactory(array($entry)));
        
        $userSynchronizer->synchronize();
        
        // retrieve user from db
        $this->userShouldBe(true, "assert the persisted user is active");
        
        // 2. if the user is not modified, the user is still present
        $userSynchronizer->synchronize();
        
        $this->userShouldBe(true, "assert the persisted user is active");
        
        // 3. if the user disappeared in ldap, the user is desactivated in database
        $userSynchronizer = $this->createUserSynchronizer(
              $this->createLdapFactory(array()));
        
        $userSynchronizer->synchronize();
        $this->userShouldBe(false, "assert the persisted user is inactive");
        
        // 3b do the same, with a non-empty ldap result
        $userSynchronizer = $this->createUserSynchronizer(
              $this->createLdapFactory(array(new Entry(self::FAKE_DN.'_2', array(
           self::USERNAME_ATTR => [ self::FAKE_LOGIN.'_2' ]
        )))));
        
        $userSynchronizer->synchronize();
        $this->userShouldBe(false, "assert the persisted user is inactive");
        
        // 4. reactive an user
        $userSynchronizer = $this->createUserSynchronizer(
              $this->createLdapFactory(array($entry)));
        
        $userSynchronizer->synchronize();
        $this->userShouldBe(true, "assert the persisted user is re-activated");
        
        
        
    }
    
    /**
     * test if : 
     * 
     * - the tested user is present in the db (see FAKE_DN)
     * - if the user has the expected username  (see self::FAKE_USERNAME)
     * - if the user is enabled/disabled, depending on the $enabled argument
     * 
     * 
     * @param boolean $enabled if the user should be enabled
     * @param string $message message to show in the test
     */
    protected function userShouldBe($enabled, $message) {
        $this->om->clear();
        $query = $this->userBindingRepository->createQueryBuilder('ub');
        $query->where($query->expr()->like('ub.ldap_dn', ':fake_dn'))
              ->setParameter('fake_dn', self::FAKE_DN);
        $users = $query->getQuery()->getResult();
        
        $this->assertEquals(1, count($users), "count there is one user with the"
              . "given fake dn");
        
        $userBinding = $users[0];
        $this->assertEquals(self::FAKE_LOGIN, $userBinding->getUser()->getUsername(),
              "test that the user has the correct username");
        $this->assertEquals($enabled, $userBinding->getUser()->isEnabled(), 
              $message);
    }
    
    
    
    
}
