<?php

namespace Chill\LdapBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use Chill\LdapBundle\Security\Factory\LdapFormFactory;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class ChillLdapBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        parent::build($container);

        // register the factory for ldap
        $extension = $container->getExtension('security');
        $extension->addSecurityListenerFactory(new LdapFormFactory());
    }
}
