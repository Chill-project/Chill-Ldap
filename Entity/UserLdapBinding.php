<?php

namespace Chill\LdapBundle\Entity;

/**
 * An entity to store information about user in the ldap directory.
 * 
 * Contains a one-to-one relationship with users.
 */
class UserLdapBinding
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $ldap_dn;

    /**
     * @var \Chill\MainBundle\Entity\User
     */
    private $user;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set ldapDn
     *
     * @param string $ldapDn
     *
     * @return UserLdapBinding
     */
    public function setLdapDn($ldapDn)
    {
        $this->ldap_dn = $ldapDn;

        return $this;
    }

    /**
     * Get ldapDn
     *
     * @return string
     */
    public function getLdapDn()
    {
        return $this->ldap_dn;
    }

    /**
     * Set user
     *
     * @param \Chill\MainBundle\Entity\User $user
     *
     * @return UserLdapBinding
     */
    public function setUser(\Chill\MainBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Chill\MainBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }
}

