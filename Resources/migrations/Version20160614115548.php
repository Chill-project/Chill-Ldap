<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Add a table to store UserLdapBindings
 */
class Version20160614115548 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE chill_ldap_user_binding_id_seq '
              . 'INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE chill_ldap_user_binding ('
              . 'id INT NOT NULL, '
              . 'user_id INT DEFAULT NULL, '
              . 'ldap_dn VARCHAR(255) NOT NULL UNIQUE, '
              . 'PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_708C6CBCA76ED395 '
              . 'ON chill_ldap_user_binding (user_id)');
        $this->addSql('CREATE UNIQUE INDEX chill_ldap_user_binding_user_id_ldap_dn '
              . 'ON chill_ldap_user_binding (user_id, ldap_dn)');
        $this->addSql('ALTER TABLE chill_ldap_user_binding '
              . 'ADD CONSTRAINT FK_708C6CBCA76ED395 FOREIGN KEY (user_id) '
              . 'REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP SEQUENCE chill_ldap_user_binding_id_seq CASCADE');
        $this->addSql('DROP TABLE chill_ldap_user_binding');
    }
}
