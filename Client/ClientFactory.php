<?php

/*
 * Chill is a software for social workers
 * Copyright (C) 2016 Champs Libres <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Chill\LdapBundle\Client;

use Symfony\Component\Ldap\LdapClient;
use Symfony\Component\Ldap\Adapter\ExtLdap\Adapter;
use Symfony\Component\Ldap\Ldap;

/**
 * Create a client to ldap
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 * @author Champs Libres <info@champs-libres.coop>
 */
class ClientFactory
{
    
    protected $host;
    protected $port;
    protected $version;
    protected $useSsl;
    protected $useStartTls;
    protected $dn;
    protected $password;
    
    public function __construct(
          $host = null, 
          $port = 389, 
          $version = 3, 
          $useSsl = false, 
          $useStartTls = false,
          $dn = null,
          $password = null
          ) 
    {
        $this->host = $host;
        $this->port = $port;
        $this->version = $version;
        $this->useSsl = $useSsl;
        $this->useStartTls = $useStartTls;
        $this->dn = $dn;
        $this->password = $password;
    }
    
    /**
     * Return a client binded to the configured dn
     * 
     * @return Ldap
     */
    public function getBindedClient($dn = null, $password = null) 
    {
        $client = $this->getClient();
        
        $client->bind(
              $dn === null ? $this->dn : $dn, 
              $password === null ? $this->password : $password
              );
        
        return $client;
    }
    
    /**
     * get a client configured to the parameters
     * 
     * the client is unauthenticated
     * 
     * @return Ldap
     */
    public function getClient()
    {
        $config = array(
           'host' => $this->host,
           'version' => $this->version,
           'port' => $this->port,
           
        );
        
        return Ldap::create('ext_ldap', $config);
    }
    
    
}
