<?php

namespace Chill\LdapBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;

/**
 * Load the bundle configuration.
 *
 */
class ChillLdapExtension extends Extension
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);
        
        $serverConfig = $config['server'];
        $container->setParameter('chill_ldap.server.host', $serverConfig['host']);
        $container->setParameter('chill_ldap.server.port', $serverConfig['port']);
        $container->setParameter('chill_ldap.server.version', $serverConfig['version']);
        $container->setParameter('chill_ldap.server.use_ssl', $serverConfig['use_ssl']);
        $container->setParameter('chill_ldap.server.use_starttls', 
              $serverConfig['use_starttls']);
        $container->setParameter('chill_ldap.server.bind_dn', $serverConfig['bind_dn']);
        $container->setParameter('chill_ldap.server.bind_password', 
              $serverConfig['bind_password']);
        
        $userQueryConfig = $config['user_query'];
        $container->setParameter('chill_ldap.user_query.dn',$userQueryConfig['dn']);
        $container->setParameter('chill_ldap.user_query.query',$userQueryConfig['query']);
        $container->setParameter('chill_ldap.user_query.username_attr',
              $userQueryConfig['username_attr']);
        
        $loader = new Loader\YamlFileLoader($container, 
              new FileLocator(__DIR__.'/../Resources/config/services'));
        $loader->load('ldap_clients.yml');
        $loader->load('synchronizers.yml');
        $loader->load('repositories.yml');
        $loader->load('security.yml');
        
    }
}
