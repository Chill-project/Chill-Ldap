<?php

namespace Chill\LdapBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('chill_ldap');

        $rootNode->children() // parent == root
              ->arrayNode('server')
                ->isRequired()
                ->cannotBeEmpty()
                ->children() // parent == server
                    ->scalarNode('host')
                      ->isRequired()
                      ->cannotBeEmpty()
                      ->example("localhost")
                      ->info("the host of the ldap directory")
                    ->end() // server children
                    ->integerNode('port')
                      ->defaultValue(389)
                      ->min(0)
                      ->max(65536)
                      ->info("the port to reach the ldap directory")
                    ->end() // server children
                    ->integerNode('version')
                      ->min(2)
                      ->max(3)
                      ->defaultValue(3)
                      ->info("the version of the ldap directory")
                     ->end() // server children
                     ->booleanNode('use_ssl')
                      ->defaultValue(false)
                      ->info("Is the use of ssl required to establish connection")
                     ->end() // server children
                     ->booleanNode('use_starttls')
                      ->defaultValue(false)
                      ->info("Is the use of startssl required to establish connection")
                     ->end() // server children
                     ->scalarNode('bind_dn')
                      ->isRequired()
                      ->cannotBeEmpty()
                      ->example("cn=user,dn=chill,dn=social")
                      ->info("the user to bind to dn directory. Required for searching existing users.")
                     ->end() // server children
                     ->scalarNode('bind_password')
                      ->isRequired()
                      ->cannotBeEmpty()
                      ->example("paSSw0rD")
                      ->info("the user's password to bind to dn directory.")
                     ->end() // server children
                    ->end() // server
                  ->end() // root children
              ->arrayNode('user_query')
                ->isRequired()
                ->cannotBeEmpty()
                ->children() // parent = user_query
                    ->scalarNode('dn')
                    ->cannotBeEmpty()
                    ->info('The DN where the query is executed')
                    ->example("ou=People,dc=champs-libres,dc=coop")
                    ->end() // user_query children
                    ->scalarNode('query')
                    ->info("The query which will allow to retrieve users")
                    ->example("(&(objectClass=inetOrgPerson)(userPassword=*))")
                    ->cannotBeEmpty()
                    ->end() // user_query children
                    ->scalarNode('username_attr')
                    ->defaultValue('cn')
                    ->info('The attribute which will provide username (=login)')
                    ->end() // user_query children
                ->end() // array user_query
              ->end() // root
              ;

        return $treeBuilder;
    }
}
