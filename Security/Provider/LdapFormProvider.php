<?php

/*
 * Chill is a software for social workers
 * Copyright (C) 2016 Champs Libres <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Chill\LdapBundle\Security\Provider;

use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserCheckerInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Ldap\LdapClientInterface;
use Symfony\Component\Ldap\Exception\ConnectionException;
use Symfony\Component\Security\Core\Authentication\Provider\UserAuthenticationProvider;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Ldap\Ldap;

/**
 * Retrieve a user binded by an ldap entry (the database and ldap must have
 * been synchronized), and check the password against the ldap.
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 * @author Champs Libres <info@champs-libres.coop>
 */
class LdapFormProvider extends UserAuthenticationProvider
{
    /**
     *
     * @var UserProviderInterface
     */
    private $userProvider;
    
    /**
     * The repository for user binding
     *
     * @var EntityRepository
     */
    protected $userBindingRepository;
    
    /**
     * The LDAP connector
     *
     * @var Ldap
     */
    protected $ldap;

    /**
     * Constructor.
     *
     * @param UserProviderInterface $userProvider               A UserProvider
     * @param UserCheckerInterface  $userChecker                A UserChecker
     * @param string                $providerKey                The provider key
     * @param LdapClientInterface   $ldap                       An Ldap client
     * @param EntityRepository      $userBindingRepository      The repository to search amongst user binded to ldap
     * @param bool                  $hideUserNotFoundExceptions Whether to hide user not found exception or not
     */
    public function __construct(
          UserProviderInterface $userProvider, 
          UserCheckerInterface $userChecker, 
          $providerKey, 
          Ldap $ldap, 
          EntityRepository $userBindingRepository,
          $hideUserNotFoundExceptions = true
          )
    {
        parent::__construct($userChecker, 
              $providerKey, 
              $hideUserNotFoundExceptions);

        $this->userProvider = $userProvider;
        $this->ldap = $ldap;
        $this->userBindingRepository = $userBindingRepository;
    }

    /**
     * {@inheritdoc}
     */
    protected function retrieveUser($username, UsernamePasswordToken $token)
    {
        if ('NONE_PROVIDED' === $username) {
            throw new UsernameNotFoundException('Username can not be null');
        }
        
        /* @var $user UserInterface */
        $user = $this->userProvider->loadUserByUsername($username);
        
        $userBinded = $this->userBindingRepository
              ->findOneBy(array('user' => $user));
        
        return $userBinded === null ? null : $user;
    }

    /**
     * {@inheritdoc}
     */
    protected function checkAuthentication(UserInterface $user, UsernamePasswordToken $token)
    {
        $username = $token->getUsername();
        $password = $token->getCredentials();

        if ('' === $password) {
            throw new BadCredentialsException('The presented password must not be empty.');
        }
        
        /* @var $userBinded \Chill\LdapBundle\Entity\UserLdapBinding */
        $userBinded = $this->userBindingRepository
              ->findOneBy(array('user' => $user));

        try {

            $this->ldap->bind($userBinded->getLdapDn(), $password);
        } catch (ConnectionException $e) {
            throw new BadCredentialsException('The presented password is invalid.');
        }
    }
}
